package com.example.SpringMVC.Fliter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(filterName = "catFilter")
public class catFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        String encoding =req.getCharacterEncoding();
        System.out.println("Before encoding "+encoding+" filter!");
        encoding="utf-8";//新编码
        req.setCharacterEncoding(encoding);
        resp.setContentType("text/html;charset="+encoding);
        System.out.println("after encoding "+encoding+" filter!");
        System.out.println("-------------------------------------");
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
