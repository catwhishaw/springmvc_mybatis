package com.example.SpringMVC.LoginServlet;

import com.example.SpringMVC.Service.NewsService;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@WebServlet(name = "DeleteNewServlet")
public class DeleteNewServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");
        int idnews=Integer.valueOf(request.getParameter("idnews"));
        System.out.println(request.getParameter("idnews"));
        NewsService newsService=new NewsService();
        newsService.DeleteNews(idnews);
        request.getRequestDispatcher("ShowNewsListServlet").forward(request,response);
    }
}
