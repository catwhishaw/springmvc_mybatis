package com.example.SpringMVC.LoginServlet;

import com.example.SpringMVC.DBdata.News;
import com.example.SpringMVC.Service.NewsService;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@Controller
@WebServlet(name = "ShowNewsListServlet")
public class ShowNewsListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");
        NewsService newsService=new NewsService();
        try {
            List<News> lsNews=newsService.QueryNews();
            request.setAttribute("lsNews",lsNews);
            request.getRequestDispatcher("ShowNewsList.jsp").forward(request,response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
