package com.example.SpringMVC.Mapper;

import com.example.SpringMVC.DBdata.News;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface NewsMapper extends Mapper<News> {


}
